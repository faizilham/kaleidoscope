.PHONY: all clean testmain testoutput libprint

all: kaleidoscope

kaleidoscope: kaleidoscope.cpp
	clang++ -g kaleidoscope.cpp -rdynamic `llvm-config --cxxflags --ldflags --system-libs --libs all` -o kaleidoscope

testmain:
	clang++ -g testmain.cpp output.o -o testmain

testoutput:
	clang++ output.o -L. -ltestprint -o testoutput

libprint:
	clang++ -c testprint.cpp -o testprint.o
	llvm-ar rc libtestprint.a testprint.o

clean:
	rm kaleidoscope
